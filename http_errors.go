/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package logicmonclient

import (
	"errors"
	"fmt"
	"net/http"
)

var ErrContinue = errors.New(http.StatusText(http.StatusContinue))
var ErrSwitchingProtocols = errors.New(http.StatusText(http.StatusSwitchingProtocols))
var ErrProcessing = errors.New(http.StatusText(http.StatusProcessing))
var ErrEarlyHints = errors.New(http.StatusText(http.StatusEarlyHints))
var ErrMultipleChoices = errors.New(http.StatusText(http.StatusMultipleChoices))
var ErrMovedPermanently = errors.New(http.StatusText(http.StatusMovedPermanently))
var ErrFound = errors.New(http.StatusText(http.StatusFound))
var ErrSeeOther = errors.New(http.StatusText(http.StatusSeeOther))
var ErrNotModified = errors.New(http.StatusText(http.StatusNotModified))
var ErrUseProxy = errors.New(http.StatusText(http.StatusUseProxy))
var ErrTemporaryRedirect = errors.New(http.StatusText(http.StatusTemporaryRedirect))
var ErrPermanentRedirect = errors.New(http.StatusText(http.StatusPermanentRedirect))
var ErrBadRequest = errors.New(http.StatusText(http.StatusBadRequest))
var ErrUnauthorized = errors.New(http.StatusText(http.StatusUnauthorized))
var ErrPaymentRequired = errors.New(http.StatusText(http.StatusPaymentRequired))
var ErrForbidden = errors.New(http.StatusText(http.StatusForbidden))
var ErrNotFound = errors.New(http.StatusText(http.StatusNotFound))
var ErrMethodNotAllowed = errors.New(http.StatusText(http.StatusMethodNotAllowed))
var ErrNotAcceptable = errors.New(http.StatusText(http.StatusNotAcceptable))
var ErrProxyAuthRequired = errors.New(http.StatusText(http.StatusProxyAuthRequired))
var ErrRequestTimeout = errors.New(http.StatusText(http.StatusRequestTimeout))
var ErrConflict = errors.New(http.StatusText(http.StatusConflict))
var ErrGone = errors.New(http.StatusText(http.StatusGone))
var ErrLengthRequired = errors.New(http.StatusText(http.StatusLengthRequired))
var ErrPreconditionFailed = errors.New(http.StatusText(http.StatusPreconditionFailed))
var ErrRequestEntityTooLarge = errors.New(http.StatusText(http.StatusRequestEntityTooLarge))
var ErrRequestURITooLong = errors.New(http.StatusText(http.StatusRequestURITooLong))
var ErrUnsupportedMediaType = errors.New(http.StatusText(http.StatusUnsupportedMediaType))
var ErrRequestedRangeNotSatisfiable = errors.New(http.StatusText(http.StatusRequestedRangeNotSatisfiable))
var ErrExpectationFailed = errors.New(http.StatusText(http.StatusExpectationFailed))
var ErrTeapot = errors.New(http.StatusText(http.StatusTeapot))
var ErrMisdirectedRequest = errors.New(http.StatusText(http.StatusMisdirectedRequest))
var ErrUnprocessableEntity = errors.New(http.StatusText(http.StatusUnprocessableEntity))
var ErrLocked = errors.New(http.StatusText(http.StatusLocked))
var ErrFailedDependency = errors.New(http.StatusText(http.StatusFailedDependency))
var ErrTooEarly = errors.New(http.StatusText(http.StatusTooEarly))
var ErrUpgradeRequired = errors.New(http.StatusText(http.StatusUpgradeRequired))
var ErrPreconditionRequired = errors.New(http.StatusText(http.StatusPreconditionRequired))
var ErrTooManyRequests = errors.New(http.StatusText(http.StatusTooManyRequests))
var ErrRequestHeaderFieldsTooLarge = errors.New(http.StatusText(http.StatusRequestHeaderFieldsTooLarge))
var ErrUnavailableForLegalReasons = errors.New(http.StatusText(http.StatusUnavailableForLegalReasons))
var ErrInternalServerError = errors.New(http.StatusText(http.StatusInternalServerError))
var ErrNotImplemented = errors.New(http.StatusText(http.StatusNotImplemented))
var ErrBadGateway = errors.New(http.StatusText(http.StatusBadGateway))
var ErrServiceUnavailable = errors.New(http.StatusText(http.StatusServiceUnavailable))
var ErrGatewayTimeout = errors.New(http.StatusText(http.StatusGatewayTimeout))
var ErrHTTPVersionNotSupported = errors.New(http.StatusText(http.StatusHTTPVersionNotSupported))
var ErrVariantAlsoNegotiates = errors.New(http.StatusText(http.StatusVariantAlsoNegotiates))
var ErrInsufficientStorage = errors.New(http.StatusText(http.StatusInsufficientStorage))
var ErrLoopDetected = errors.New(http.StatusText(http.StatusLoopDetected))
var ErrNotExtended = errors.New(http.StatusText(http.StatusNotExtended))
var ErrNetworkAuthenticationRequired = errors.New(http.StatusText(http.StatusNetworkAuthenticationRequired))

var httpStatusToError = map[int]error{
	100: ErrContinue,
	101: ErrSwitchingProtocols,
	102: ErrProcessing,
	103: ErrEarlyHints,
	300: ErrMultipleChoices,
	301: ErrMovedPermanently,
	302: ErrFound,
	303: ErrSeeOther,
	304: ErrNotModified,
	305: ErrUseProxy,
	307: ErrTemporaryRedirect,
	308: ErrPermanentRedirect,
	400: ErrBadRequest,
	401: ErrUnauthorized,
	402: ErrPaymentRequired,
	403: ErrForbidden,
	404: ErrNotFound,
	405: ErrMethodNotAllowed,
	406: ErrNotAcceptable,
	407: ErrProxyAuthRequired,
	408: ErrRequestTimeout,
	409: ErrConflict,
	410: ErrGone,
	411: ErrLengthRequired,
	412: ErrPreconditionFailed,
	413: ErrRequestEntityTooLarge,
	414: ErrRequestURITooLong,
	415: ErrUnsupportedMediaType,
	416: ErrRequestedRangeNotSatisfiable,
	417: ErrExpectationFailed,
	418: ErrTeapot,
	421: ErrMisdirectedRequest,
	422: ErrUnprocessableEntity,
	423: ErrLocked,
	424: ErrFailedDependency,
	425: ErrTooEarly,
	426: ErrUpgradeRequired,
	428: ErrPreconditionRequired,
	429: ErrTooManyRequests,
	431: ErrRequestHeaderFieldsTooLarge,
	451: ErrUnavailableForLegalReasons,
	500: ErrInternalServerError,
	501: ErrNotImplemented,
	502: ErrBadGateway,
	503: ErrServiceUnavailable,
	504: ErrGatewayTimeout,
	505: ErrHTTPVersionNotSupported,
	506: ErrVariantAlsoNegotiates,
	507: ErrInsufficientStorage,
	508: ErrLoopDetected,
	510: ErrNotExtended,
	511: ErrNetworkAuthenticationRequired,
}

type LMError struct {
	ErrorCode    int32  `json:"errorCode"`
	ErrorMessage string `json:"errorMessage"`
	ErrorDetail  string `json:"errorDetail"`
}

func (e *LMError) Error() string {
	return fmt.Sprintf(
		"logicmonitor error %d: %s (%s)",
		e.ErrorCode,
		e.ErrorMessage,
		e.ErrorDetail,
	)
}

// Taken from:
// * https://www.logicmonitor.com/support/api-error-codes
// * https://www.logicmonitor.com/support/rest-api-status-codes
var ErrLMRecordExists = errors.New("The record already exists (600)")
var ErrLMServerBusy = errors.New("Server is busy")
var ErrLMInternal = errors.New("Internal error (1001)")
var ErrLMNoSuchDeviceGroup = errors.New("No such DeviceGroup")
var ErrLMBadRequest = errors.New("Bad request")
var ErrLMDeviceGroupNotDeleted = errors.New("Device Group was not deleted")
var ErrLMNoSuchDeviceDataSource = errors.New("No such Device DataSource")
var ErrLMInstanceGroupNotCreated = errors.New("Instance group cannot be created")
var ErrLMNoSuchInstanceGroup = errors.New("No such InstanceGroup")
var ErrLMNoSuchDataSource = errors.New("No such DataSource")
var ErrLMNoSuchInstance = errors.New("No such Instance")
var ErrLMAppliesTo = errors.New("There is a syntax error in the appliesTo field.")
var ErrLMNoSuchCollector = errors.New("No such Collector")
var ErrLMNoSuchDatapoint = errors.New("No such Datapoint")
var ErrLMCannotImportLogicModule = errors.New("Cannot import LogicModule")
var ErrLMPermissionDenied = errors.New("Permission denied")
var ErrLMNoSuchSDT = errors.New("No such SDT")
var ErrLMNoSuchWidget = errors.New("No such widget")
var ErrLMNoSuchCompany = errors.New("No such company (1065)")
var ErrLMNoSuchRecord = errors.New("No such record (1069)")
var ErrLMCollectorInactive = errors.New("The collector isn’t active")
var ErrLMReportTooLarge = errors.New("Report too large")
var ErrLMRequestEntityTooLarge = errors.New("The request entity/report is too large (1075)")
var ErrLMTemplateTooLarge = errors.New("Template too large")
var ErrLMInvalidMacros = errors.New("Invalid Macros / Bad request")
var ErrLMUploadDocxFile = errors.New("Please upload a .docx file")
var ErrLMReportTemplateNotUploaded = errors.New("The report template could not be uploaded")
var ErrLMRateExceed = errors.New("Rate exceed")
var ErrLMTooManyRequests = errors.New("Too many requests")
var ErrLMQueryTimeout = errors.New("Query timed out")
var ErrLMResourceDependency = errors.New("Resource Dependency (1104)")
var ErrLMPartialSuccess = errors.New("Partial success / The update was partially successful (1201)")
var ErrLMTaskRunning = errors.New("The task is running")
var ErrLMNoContent = errors.New("No Content")
var ErrLMSaveFailed = errors.New("Save failed")
var ErrLMNoValidLocations = errors.New("All Locations are disable / No valid locations selected")
var ErrLMReportInProgress = errors.New("Report in progress")
var ErrLMBadRequest2 = errors.New("Bad request")
var ErrLMAuthenticationFailed = errors.New("Authentication failed")
var ErrLM2FARequired = errors.New("Two-factor authentication is required.")
var ErrLMNoSuchRecord2 = errors.New("No such record (1404)")
var ErrLMRecordExists2 = errors.New("The record already exists (1409")
var ErrLMFalsePrecondition = errors.New("The supplied precondition evaluated to false")
var ErrLMRequestEntityTooLarge2 = errors.New("The request entity is too large (1413)")
var ErrLMInternal2 = errors.New("Internal error (1500)")
var ErrLM2FANotEnabled = errors.New("User must enable two-factor authentication")
var ErrLMIncorrectPassword = errors.New("Incorrect password")
var ErrLMResourceDependency2 = errors.New("Resource Dependency (14001)")
var ErrLMPartialSuccess2 = errors.New("Partial success (14002)")
var ErrLMInvalidProperty = errors.New("Properties must start with a letter to be valid.")
var ErrLMQueryStringParams = errors.New("Error in query string parameters")
var ErrLMNoSuchCompany2 = errors.New("No Such Company (14042)")

var lmStatusToError = map[int]error{
	600:   ErrLMRecordExists,
	1000:  ErrLMServerBusy,
	1001:  ErrLMInternal,
	1004:  ErrLMNoSuchDeviceGroup,
	1007:  ErrLMBadRequest,
	1010:  ErrLMDeviceGroupNotDeleted,
	1013:  ErrLMNoSuchDeviceDataSource,
	1014:  ErrLMInstanceGroupNotCreated,
	1015:  ErrLMNoSuchInstanceGroup,
	1022:  ErrLMNoSuchDataSource,
	1027:  ErrLMNoSuchInstance,
	1031:  ErrLMAppliesTo,
	1033:  ErrLMNoSuchCollector,
	1037:  ErrLMNoSuchDatapoint,
	1040:  ErrLMCannotImportLogicModule,
	1041:  ErrLMPermissionDenied,
	1048:  ErrLMNoSuchSDT,
	1058:  ErrLMNoSuchWidget,
	1065:  ErrLMNoSuchCompany,
	1069:  ErrLMNoSuchRecord,
	1073:  ErrLMCollectorInactive,
	1074:  ErrLMReportTooLarge,
	1075:  ErrLMRequestEntityTooLarge,
	1076:  ErrLMTemplateTooLarge,
	1077:  ErrLMInvalidMacros,
	1078:  ErrLMUploadDocxFile,
	1079:  ErrLMReportTemplateNotUploaded,
	1091:  ErrLMRateExceed,
	1100:  ErrLMTooManyRequests,
	1101:  ErrLMQueryTimeout,
	1104:  ErrLMResourceDependency,
	1201:  ErrLMPartialSuccess,
	1202:  ErrLMTaskRunning,
	1204:  ErrLMNoContent,
	1301:  ErrLMSaveFailed,
	1303:  ErrLMNoValidLocations,
	1313:  ErrLMReportInProgress,
	1400:  ErrLMBadRequest2,
	1401:  ErrLMAuthenticationFailed,
	1403:  ErrLM2FARequired,
	1404:  ErrLMNoSuchRecord2,
	1409:  ErrLMRecordExists2,
	1412:  ErrLMFalsePrecondition,
	1413:  ErrLMRequestEntityTooLarge2,
	1500:  ErrLMInternal2,
	2403:  ErrLM2FANotEnabled,
	3403:  ErrLMIncorrectPassword,
	14001: ErrLMResourceDependency2,
	14002: ErrLMPartialSuccess2,
	14003: ErrLMInvalidProperty,
	14004: ErrLMQueryStringParams,
	14042: ErrLMNoSuchCompany2,
}
