/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package logicmonclient

import "testing"

func TestGeneratesReferenceAuthentication(t *testing.T) {
	cred := NewLMv1TokenBasedCredential(
		"84GKZTJ7UNB43U84w8qC",
		"(8f~NrDi$5mr{6+S82W4XkB~%Ih8)6SItNX3XfKH",
	)
	verb := "GET"
	resource := "/dashboard/widgets"
	epoch := "1502813538347"

	// From https://www.logicmonitor.com/support/rest-api-developers-guide/overview/api-troubleshooting
	expected := "LMv1 84GKZTJ7UNB43U84w8qC:MzFmZWJlZmJlMTMxNmI3NjFmYTE5OTgyZWFlODU3ZDhiOTdmYWNiYzc2NGViOTFlNWYwNjJhZjQxNWNiZDMzYQ==:1502813538347"
	actual := generateAuthorization(
		epoch,
		verb,
		resource,
		[]byte{},
		cred,
	)

	if expected != actual {
		t.Errorf("authorization does not match reference!")
	}
}
