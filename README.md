# logicmonclient

A client for the LogicMonitor REST API v3.

## Example

```go
package main

import (
    "context"
    "net/http"
    "os"

    lmc "idio.link/go/logicmonclient"
)

func main() {
	accountName := os.GetEnv("LOGICMON_ACCOUNT")
	accessId := os.GetEnv("LOGICMON_ACCESSID")
	accessKey := os.GetEnv("LOGICMON_ACCESSKEY")

	// set up connection
	httpTransport := &http.Transport{
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: false},
		TLSHandshakeTimeout:   15 * time.Second,
		IdleConnTimeout:       30 * time.Minute,
		ResponseHeaderTimeout: 180 * time.Second,
		ExpectContinueTimeout: 10 * time.Second,
	}
	httpClient := &http.Client{
		Transport: httpTransport,
		Timeout:   30 * time.Minute,
	}

    ctx := context.Background()

	cred := lmc.NewLMv1TokenBasedCredential(accessId, accessKey)
	client := lmc.NewLMClient(accountName, ctx, httpClient, cred)

	netflowFilter := lmc.LMNetflowFilter{
        Direction: lmc.LMFlowDirectionEgress,
		IPVersion: lmc.LMIPVersion4,
		Protocol:  lmc.LMProtocolGRE,
		Top:       lmc.LMTopCount100,
	}

    devPager := client.GetImmediateDeviceList(364)
    for dev := range devPager.Iter() {
        if ctx.Err() != nil {
            break
        }
        graph, err := client.GetTopTalkersGraph(
            dev.Id,
            window.Start.UnixMilli()/1000,
            window.End.UnixMilli()/1000,
            netflowFilter,
        )
        if err != nil {
            log.Printf("WARN: make edges: device %s: %v", dev.DisplayName, err)
            continue
        }

        // do things
    }
}
```
